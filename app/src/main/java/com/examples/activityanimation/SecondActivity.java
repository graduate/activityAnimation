//Gaurab Dahal
package com.examples.activityanimation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class SecondActivity extends AppCompatActivity {
    RelativeLayout rl1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        rl1 = (RelativeLayout) findViewById(R.id.rl1);
        rl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animate();
            }
        });
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        animate();
    }

    private void animate(){
        finish();
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        MainActivity.sp.play(MainActivity.sound, 1, 1, 0, 0, 1);
    }
}

//Gaurab Dahal

package com.examples.activityanimation;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {
    MyView mv;
    static int sound;
    public static SoundPool sp;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Make full screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                             WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            MainActivity.sp=new SoundPool(2, AudioManager.STREAM_MUSIC,0);
        }else{
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            MainActivity.sp = new SoundPool.Builder()
                    .setAudioAttributes(attributes)
                    .build();
        }

        mv = new MyView(this);
        setContentView(mv);
    }

    public class MyView extends View {
        Paint paint;
        boolean initialized;

        public MyView(Context context) {
            super(context);
            initialized = false;
            sound = sp.load(context,R.raw.transition,1);
        }
        private void InitGraphics (Canvas canvas) {
           paint = new Paint();
           initialized = true;
        }

        @Override
        protected void onDraw (Canvas canvas) {
            InitGraphics(canvas);
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.WHITE);
            canvas.drawPaint(paint);

            // Use Color.parseColor to define HTML colors
            paint.setColor(Color.parseColor("#CD5C5C"));
            canvas.drawCircle (canvas.getWidth()/2, canvas.getHeight()/2, canvas.getWidth()/4, paint);
            invalidate();
        }
        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            int action = event.getAction();
            if (action == MotionEvent.ACTION_UP) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, SecondActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                MainActivity.sp.play(sound,1,1,0,0,1);
                //finish(); // use this to close this Activity permanently
            }
            return true; // to indicate we have handled this event
        }
    }
}
